import {Routes} from '@angular/router';
import {AdminComponent} from './layout/admin/admin.component';

const AppRoutes: Routes = [
  { path: '',
   component: AdminComponent,
   children: [
    {
      path: '',
      redirectTo: 'listusers',
      pathMatch: 'full'
    },
    {
      path: 'listusers',
      loadChildren: './pages/list-users/list-users.module#ListUsersModule'
    }]
  }
];
