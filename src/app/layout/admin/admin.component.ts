import { Component, OnInit } from '@angular/core';
import { SuppliersService } from '../../services/http/suppliers.service';
import { DishesService } from '../../services/http/dishes.service';
import * as moment from 'moment';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor() { }
  refreshData() {
    const serviceRefresh = new DishesService();
    serviceRefresh.getDishes();
  }
  ngOnInit() {
  }

}
