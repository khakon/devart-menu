import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Repository } from '../base/repository';
import { Menu } from '../../models/menu-data';

@Injectable()
export class MenusService extends Repository {

  constructor() {
    super('menus', []);
  }

  saveMenus(menus: Menu[]): void {
    super.saveEntities(menus);
  }

  getMenus(): Observable<Menu[]> {
    return super.getEntities();
  }

  getNewId(): number {
    return super.getNewId();
  }

  getMenu(menuId: number): Menu {
    return super.getEntity(menuId);
  }
}
