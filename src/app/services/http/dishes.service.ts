
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { Dish } from '../../models/dish';
import { ListDishes } from '../../data/dishes';
import { Repository } from '../base/repository';


@Injectable()
export class DishesService extends Repository {

  constructor() { super('dishes', ListDishes); }

  getDishes(): Observable<Dish[]> {
    return super.getEntities();
  }

  saveDishes(dishes: Dish[]): void {
    super.saveEntities(dishes);
  }

  getNewId(): number {
    return super.getNewId();
  }
  getDish(dishId: number): Dish {
    return super.getEntity(dishId);
  }
}
