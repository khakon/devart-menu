import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { User } from '../../models/user';
import { ListUsers } from '../../data/users';
import { Repository } from '../base/repository';


@Injectable()
export class UsersService extends Repository {

  constructor() { super('users', ListUsers); }

  getUsers(): Observable<User[]> {
    return super.getEntities();
  }

  saveUsers(users: User[]): void {
    super.saveEntities(users);
  }

  getNewId(): number {
    return super.getNewId();
  }

  getUser(userId: number): User {
    return super.getEntity(userId);
  }

  setCurrentUser(user: User): void {
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  getCurrentUser(): any {
    const entityString = localStorage.getItem('currentUser');
    const entity = entityString ? JSON.parse(entityString) : {};
    return entity;
  }

}
