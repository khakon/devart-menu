import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { Dinner } from '../../models/dinner';
import { Repository } from '../base/repository';

@Injectable()
export class DinnersService extends Repository {

  constructor() { super('dinners', []); }

  getDinners(): Observable<Dinner[]> {
    return super.getEntities();
  }

  saveDinners(dinners: Dinner[]): void {
    super.saveEntities(dinners);
  }

  saveDinner(dinner: Dinner) {
    super.saveEntity(dinner);
  }

  deleteDinner(dinner: Dinner) {
    super.deleteEntity(dinner.id);
  }

  reloadDinners(): Dinner[] {
    return [];
  }

  getNewId(): number {
    return super.getNewId();
  }
  getDinner(dishId: number): Dinner {
    return super.getEntity(dishId);
  }

}
