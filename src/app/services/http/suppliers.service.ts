import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { Supplier } from '../../models/supplier';
import { ListSuppliers } from '../../data/suppliers';
import { Repository } from '../base/repository';


@Injectable()
export class SuppliersService extends Repository {

  constructor() { super('suppliers', ListSuppliers); }

  saveSuppliers(suppliers: Supplier[]): void {
    super.saveEntities(suppliers);
  }

  getSuppliers(): Observable<Supplier[]> {
    return super.getEntities();
  }

  getNewId(): number {
    return super.getNewId();
  }

  getSupplier(supId: number): Supplier {
    return super.getEntity(supId);
  }
}
