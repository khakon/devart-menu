import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export class Repository {

  private _storageKey: string;
  private _list: any[];
  constructor(storageKey: string, list: any[]) {
    this._storageKey = storageKey;
    this._list = list;
  }

  getEntities(): Observable<any[]> {
    const entitiesString = localStorage.getItem(this._storageKey);
    const entities = entitiesString ? JSON.parse(entitiesString) : this.reload();
    return Observable.of(entities);
  }

  saveEntities(entities: any[]): void {
    localStorage.setItem(this._storageKey, JSON.stringify(entities));
  }

  saveEntity(entity: any): void {
    this.getEntities().subscribe(entities => {
      entities.push(entity);
      this.saveEntities(entities);
    });
  }

  deleteEntity(id: number): void {
    this.getEntities().subscribe(entities => {
      entities.splice(entities.findIndex(item => item.id === id), 1);
      this.saveEntities(entities);
    });
  }

  getNewId(): number {
    const entitiesString = localStorage.getItem(this._storageKey);
    const entities = entitiesString ? JSON.parse(entitiesString) : this.reload();
    return entities.reduce((maxId, item) => item.id > maxId ? item.id : maxId, 0) + 1;
  }

  getEntity(id: number): any {
    const entitiesString = localStorage.getItem(this._storageKey);
    const entities = entitiesString ? JSON.parse(entitiesString) : this.reload();
    return entities.find((item) => item.id === id);
  }

  reload(): any[] {
    this.saveEntities(this._list);
    return this._list;
  }
}
