import { Component, OnInit } from '@angular/core';
import { DishesService } from '../../../services/http/dishes.service';
import { SuppliersService } from '../../../services/http/suppliers.service';
import { MenusService } from '../../../services/http/menus.service';
import { UsersService } from '../../../services/http/users.service';
import { DinnersService } from '../../../services/http/dinners.service';
import { Dish } from '../../../models/dish';
import { Menu } from '../../../models/menu-data';
import { User } from '../../../models/user';
import { Dinner } from '../../../models/dinner';
import { Supplier } from '../../../models/supplier';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/zip';
import * as moment from 'moment';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css']
})
export class MenusComponent implements OnInit {

  suppliers: Supplier[];
  dishes: Dish[];
  dishesList: Dish[];
  menus: Menu[];
  appointments: Appointment[];
  selectedMenu: Menu;
  currentDate: Date = new Date(2017, 4, 11);
  cellContextMenuItems: any[];
  contextMenuCellData: any;
  popupVisible: boolean;
  popupSuppliersVisible: boolean;
  popupDishesVisible: boolean;
  selectedSuppliers: any[] = [];
  selectedDishes: any[] = [];
  currentUser: User;

  constructor(private dishesData: DishesService, private suppliersData: SuppliersService,
    private menuData: MenusService, private userData: UsersService, private dinnerData: DinnersService) {
    this.cellContextMenuItems = [
      { text: 'Добавить меню', onItemClick: () => this.createMenu() }];
    this.popupVisible = false;
    this.popupSuppliersVisible = false;
    this.popupDishesVisible = false;
    this.selectedSuppliers = [];
    this.selectedDishes = [];
    this.refresh();
  }

  ngOnInit() {
  }
  /////////////////// Dinner
  onAppointmentClick(event) {
    event.cancel = true;
    this.editMenu(event.appointmentData);
  }
  createMenu() {
    // console.log(this.contextMenuCellData.startDate);
    // this.selectedMenu = new Menu();
    // this.selectedMenu.period = this.contextMenuCellData.startDate;
    // moment.lang('ru');
    // this.selectedMenu.name = moment(this.selectedMenu.period).format('dddd, MMMM DD YYYY');
    // this.selectedMenu.supplier = new Supplier();
    // this.dishesList = [];
    // this.selectedMenu.dishes = [];
    // this.popupVisible = true;
  }
  editMenu(appointment: Appointment): void {
    this.selectedMenu = this.menus.find(menu => menu.id === appointment.menuId);
    this.dishesList = this.selectedMenu.dishes;
    this.selectedSuppliers.push(this.selectedMenu.supplier);
    this.popupVisible = true;
  }
  deleteMenufromSheduler(appointment: Appointment): void {
    const menuToDelete = this.menus.find(menu => menu.id === appointment.menuId);
    this.menus.splice(this.menus.indexOf(menuToDelete), 1);
    this.refresh();
  }
  saveDinner() {
    this.popupVisible = false;
    this.selectedDishes.forEach(dish => {
      const dinner = new Dinner(this.selectedMenu, dish);
      dinner.client = this.currentUser;
      dinner.id = this.dinnerData.getNewId();
      this.dinnerData.saveDinner(dinner);
    });
    this.refresh();
  }
  deleteMenu() {
    this.menus.splice(this.menus.indexOf(this.selectedMenu), 1);
    this.popupVisible = false;
  }
  close() {
    this.popupVisible = false;
  }
  ////////////////////////////////////
  refresh() {
    Observable.zip(this.suppliersData.getSuppliers(),
      this.dishesData.getDishes(),
      this.menuData.getMenus(),
    ).subscribe(([suppliers, dishes, menus]) => {
      this.suppliers = suppliers;
      this.dishes = dishes;
      this.menus = menus;
      console.log(menus);
      this.appointments = menus.map(menu => new Appointment(menu));
      this.selectedSuppliers = [];
      this.currentUser = this.userData.getCurrentUser();
    }
    );
  }
}

export class Appointment {
  text: string;
  startDate: Date;
  menuId: number;
  allDay?: boolean;
  constructor(menu: Menu) {
    this.text = menu.supplier.name;
    this.startDate = menu.period;
    this.menuId = menu.id;
  }
}
