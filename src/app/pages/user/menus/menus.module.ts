import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenusComponent } from './menus.component';
import { RouterModule, Routes } from '@angular/router';

import {
  DxContextMenuModule, DxNumberBoxModule, DxTextBoxModule, DxListModule,
  DxDataGridModule, DxButtonModule, DxLoadPanelModule, DxPopupModule, DxSelectBoxModule,
  DxTextAreaModule, DxFormModule, DxSchedulerModule, DxTemplateModule
} from 'devextreme-angular';

import { DishesService } from '../../../services/http/dishes.service';
import { SuppliersService } from '../../../services/http/suppliers.service';
import { MenusService } from '../../../services/http/menus.service';
import { UsersService } from '../../../services/http/users.service';
import { DinnersService } from '../../../services/http/dinners.service';

export const MenuRoutes: Routes = [
  {
    path: '',
    component: MenusComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MenuRoutes),
    DxButtonModule,
    DxDataGridModule,
    DxLoadPanelModule,
    DxPopupModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxFormModule,
    DxTemplateModule,
    DxSchedulerModule,
    DxListModule,
    DxTextBoxModule,
    DxNumberBoxModule,
    DxContextMenuModule,
  ],
  providers: [SuppliersService, DishesService, MenusService, UsersService, DinnersService],
  declarations: [MenusComponent]
})

export class MenusModule { }

