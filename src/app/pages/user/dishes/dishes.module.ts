import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DishesComponent } from './dishes.component';
import { RouterModule, Routes } from '@angular/router';

import { DxNumberBoxModule, DxTextBoxModule, DxListModule, DxDataGridModule,
   DxButtonModule, DxLoadPanelModule, DxPopupModule, DxSelectBoxModule, DxTextAreaModule,
   DxFormModule, DxSchedulerModule, DxTemplateModule } from 'devextreme-angular';

import { DishesService } from '../../../services/http/dishes.service';
import { SuppliersService } from '../../../services/http/suppliers.service';


export const DishesRoutes: Routes = [
  {
    path: '',
    component: DishesComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DishesRoutes),
    DxButtonModule,
    DxDataGridModule,
    DxLoadPanelModule,
    DxPopupModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxFormModule,
    DxTemplateModule,
    DxSchedulerModule,
    DxListModule,
    DxTextBoxModule,
    DxNumberBoxModule,
  ],
  providers: [SuppliersService, DishesService],
  declarations: [DishesComponent]
})
export class DishesModule { }

