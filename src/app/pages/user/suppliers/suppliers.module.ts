import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuppliersComponent } from './suppliers.component';
import {RouterModule, Routes} from '@angular/router';

import { DxTextBoxModule, DxListModule, DxDataGridModule, DxButtonModule,
         DxLoadPanelModule, DxPopupModule, DxSelectBoxModule, DxTextAreaModule,
         DxFormModule, DxSchedulerModule, DxTemplateModule } from 'devextreme-angular';
import { SuppliersService } from '../../../services/http/suppliers.service';

export const SuppliersRoutes: Routes = [
  {
    path: '',
    component: SuppliersComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SuppliersRoutes),
    DxButtonModule,
    DxDataGridModule,
    DxLoadPanelModule,
    DxPopupModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxFormModule,
    DxTemplateModule,
    DxSchedulerModule,
    DxListModule,
    DxTextBoxModule,
  ],
  providers: [SuppliersService],
  declarations: [SuppliersComponent]
})
export class SuppliersModule { }
