import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersService } from '../../../services/http/users.service';
import { UsersAuthComponent } from './users-auth.component';
import { DxButtonModule, DxLookupModule } from 'devextreme-angular';

export const UsersRoutes: Routes = [
  {
    path: '',
    component: UsersAuthComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UsersRoutes),
    DxButtonModule,
    DxLookupModule,
  ],
  providers: [UsersService],
  declarations: [UsersAuthComponent]
})
export class UsersAuthModule { }

