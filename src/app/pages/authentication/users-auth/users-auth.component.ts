import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../../../services/http/users.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-users-auth',
  templateUrl: './users-auth.component.html',
  styleUrls: ['./users-auth.component.css']
})
export class UsersAuthComponent implements OnInit {

  users: User[];
  selectedUser: User;
  popupVisible: boolean;

  constructor(private usersData: UsersService, private router: Router) {
    this.refresh();
  }

  ngOnInit() {
  }

  valueChanged(data) {
    this.selectedUser = this.users.find(user => user.id === data.value);
  }
  logIn() {
    this.usersData.setCurrentUser(this.selectedUser);
    if (this.selectedUser.name === 'Admin') {
      this.router.navigate(['/admin']);
    } else {
      this.router.navigate(['/user']);
    }
  }
  refresh() {
    this.usersData.getUsers().subscribe(data => this.users = data);
  }
}
