import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu.component';
import { RouterModule, Routes } from '@angular/router';

import { DxContextMenuModule, DxNumberBoxModule, DxTextBoxModule, DxListModule,
         DxDataGridModule, DxButtonModule, DxLoadPanelModule, DxPopupModule, DxSelectBoxModule,
         DxTextAreaModule, DxFormModule, DxSchedulerModule, DxTemplateModule } from 'devextreme-angular';

import { DishesService } from '../../../services/http/dishes.service';
import { SuppliersService } from '../../../services/http/suppliers.service';
import { MenusService } from '../../../services/http/menus.service';

export const MenuRoutes: Routes = [
  {
    path: '',
    component: MenuComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MenuRoutes),
    DxButtonModule,
    DxDataGridModule,
    DxLoadPanelModule,
    DxPopupModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxFormModule,
    DxTemplateModule,
    DxSchedulerModule,
    DxListModule,
    DxTextBoxModule,
    DxNumberBoxModule,
    DxContextMenuModule,
  ],
  providers: [SuppliersService, DishesService, MenusService],
  declarations: [MenuComponent]
})

export class MenuModule { }
