import { Component, OnInit } from '@angular/core';
import { DishesService } from '../../../services/http/dishes.service';
import { SuppliersService } from '../../../services/http/suppliers.service';
import { MenusService } from '../../../services/http/menus.service';
import { Dish } from '../../../models/dish';
import { Menu } from '../../../models/menu-data';
import { Supplier } from '../../../models/supplier';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/zip';
import * as moment from 'moment';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  suppliers: Supplier[];
  dishes: Dish[];
  dishesList: Dish[];
  menus: Menu[];
  appointments: Appointment[];
  selectedMenu: Menu;
  currentDate: Date = new Date();
  cellContextMenuItems: any[];
  contextMenuCellData: any;
  popupVisible: boolean;
  popupSuppliersVisible: boolean;
  popupDishesVisible: boolean;
  selectedSuppliers: any[] = [];
  selectedDishes: any[] = [];

  constructor(private dishesData: DishesService, private suppliersData: SuppliersService, private menuData: MenusService) {
    this.cellContextMenuItems = [
      { text: 'Добавить меню', onItemClick: () => this.createMenu() }];
    this.popupVisible = false;
    this.popupSuppliersVisible = false;
    this.popupDishesVisible = false;
    this.selectedSuppliers = [];
    this.selectedDishes = [];
    this.refresh();
  }

  ngOnInit() {
  }
  /////////////////// Menu
  createMenu() {
    console.log(this.contextMenuCellData.startDate);
    this.selectedMenu = new Menu();
    this.selectedMenu.period = this.contextMenuCellData.startDate;
    moment.lang('ru');
    this.selectedMenu.name = moment(this.selectedMenu.period).format('dddd, MMMM DD YYYY');
    this.selectedMenu.day = moment(this.selectedMenu.period).format('dddd');
    this.selectedMenu.week = moment(this.selectedMenu.period).week();
    this.selectedMenu.supplier = new Supplier();
    this.dishesList = [];
    this.selectedMenu.dishes = [];
    this.popupVisible = true;
  }
  editMenu(appointment: Appointment): void {
    this.selectedMenu = this.menus.find(menu => menu.id === appointment.menuId);
    this.dishesList = this.selectedMenu.dishes;
    this.selectedSuppliers.push(this.selectedMenu.supplier);
    this.popupVisible = true;
  }
  deleteMenufromSheduler(appointment: Appointment): void {
    const menuToDelete = this.menus.find(menu => menu.id === appointment.menuId);
    this.menus.splice(this.menus.indexOf(menuToDelete), 1);
    this.refresh();
  }
  saveMenu() {
    this.popupVisible = false;
    if (this.selectedMenu.id === 0) {
      this.selectedMenu.id = this.menuData.getNewId();
    } else {
      this.menus.splice(this.menus.indexOf(this.selectedMenu), 1);
    }
    this.menus.push(this.selectedMenu);
    this.menuData.saveMenus(this.menus);
    this.refresh();
  }
  deleteMenu() {
    this.menus.splice(this.menus.indexOf(this.selectedMenu), 1);
    this.popupVisible = false;
  }
  closeMenu() {
    this.popupVisible = false;
  }
  onContextMenuItemClick(e) {
    e.itemData.onItemClick(e.itemData);
  }
  onCellContextMenu(e) {
    this.contextMenuCellData = e.cellData;
  }
  ////////////////////////////////// Supplier
  addSupplier() {
    if (this.selectedSuppliers.length > 0) { this.selectedMenu.supplier = this.selectedSuppliers[0]; }
    this.dishesList = this.dishes.filter(dish => dish.supplier.id === this.selectedMenu.supplier.id);
    this.popupVisible = true;
    this.popupSuppliersVisible = false;
    this.selectedMenu.name = `${this.selectedMenu.name}  ${this.selectedMenu.supplier.name}`;
  }
  selectSupplier() {
    this.popupSuppliersVisible = true;
  }
  closeSupplier() {
    this.popupSuppliersVisible = false;
    this.popupVisible = true;
  }
  ////////////////////////////////// Dishes
  addDish() {
    if (this.selectedDishes.length > 0) {
      this.selectedMenu.dishes = this.selectedDishes;
      this.popupVisible = true;
      this.popupDishesVisible = false;
    }
  }
  selectDish() {
    this.popupDishesVisible = true;
  }
  closeDish() {
    this.popupDishesVisible = false;
    this.popupVisible = true;
  }
  ////////////////////////////////////
  refresh() {
    Observable.zip(this.suppliersData.getSuppliers(),
      this.dishesData.getDishes(),
      this.menuData.getMenus())
      .subscribe(([suppliers, dishes, menus]) => {
        this.suppliers = suppliers;
        this.dishes = dishes;
        this.menus = menus;
        if (menus.length > 0) {
          this.currentDate = menus.reduce((res, item) => {
            return moment(item.period) > moment(res) ? item.period : res;
          }, new Date(1, 1, 1));
        }
        this.appointments = menus.map(menu => new Appointment(menu));
        this.selectedSuppliers = [];
      }
      );
  }
}

export class Appointment {
  text: string;
  startDate: Date;
  menuId: number;
  allDay?: boolean;
  constructor(menu: Menu) {
    this.text = menu.supplier.name;
    this.startDate = menu.period;
    this.menuId = menu.id;
  }
}
