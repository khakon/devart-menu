import { Component, OnInit } from '@angular/core';
import { DinnersService } from '../../../services/http/dinners.service';
import { UsersService } from '../../../services/http/users.service';
import { Dinner } from '../../../models/dinner';
import { User } from '../../../models/user';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/zip';

@Component({
  selector: 'app-dinners',
  templateUrl: './dinners.component.html',
  styleUrls: ['./dinners.component.css']
})
export class DinnersComponent implements OnInit {

  week: number;
  weekText: string;
  user: User;
  dinners: Dinner[];
  loadingVisible: boolean;
  selectedRow: any;
  checkAll: boolean;
  lastRowCLickedId: number;
  selectedDinner: Dinner;
  expanded: boolean;
  constructor(private dinnersData: DinnersService, private usersData: UsersService) {
    this.dinners = [];
    this.week = 0;
    this.user = this.usersData.getCurrentUser();
    this.refresh();
  }
  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'weekInfo'
    }, {
        widget: 'dxButton',
        options: { icon: 'pulldown', onClick: this.refresh.bind(this) },
        location: 'before'
      }, {
        location: 'before',
        widget: 'dxButton',
        options: {
          width: 136,
          text: 'Свернуть',
          onClick: this.collapseAllClick.bind(this)
        }
      });
  }
  rowClickEvent(e) {
    console.log(e.element);
    const rows = e.component.getSelectedRowsData();
    if (this.lastRowCLickedId === e.rowIndex) {
      this.lastRowCLickedId = e.rowIndex;
      console.log('double');
      rows.then((data) => {
        const row = data[0];
        this.selectedDinner = row;
      });
    } else {
      console.log('single');
    }
    this.lastRowCLickedId = e.rowIndex;
  }
  collapseAllClick(e) {
    this.expanded = !this.expanded;
    e.component.option({
      text: this.expanded ? 'Свернуть' : 'Развернуть'
    });
  }
  checkAllClick(e) {
    this.checkAll = e.value;
    this.refresh();
  }
  ngOnInit() {
  }
  refresh() {
    Observable.zip(this.dinnersData.getDinners()).subscribe(([dinners]) => {
      if (dinners.length > 0) {
        if (this.week === 0) {
          this.week = dinners.reduce((res, item) => item.week > res ? item.week : res, 0);
        }
        this.dinners = dinners.filter((item => item.week === this.week));
        this.weekText = `Неделя ${this.week}`;
      }
    }
    );
  }
}
