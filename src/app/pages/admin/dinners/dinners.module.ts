import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DinnersComponent } from './dinners.component';
import { RouterModule, Routes } from '@angular/router';

import {
  DxNumberBoxModule, DxTextBoxModule, DxListModule, DxDataGridModule,
  DxButtonModule, DxLoadPanelModule, DxPopupModule, DxSelectBoxModule, DxTextAreaModule,
  DxFormModule, DxSchedulerModule, DxTemplateModule
} from 'devextreme-angular';

import { DishesService } from '../../../services/http/dishes.service';
import { SuppliersService } from '../../../services/http/suppliers.service';
import { DinnersService } from '../../../services/http/dinners.service';
import { UsersService } from '../../../services/http/users.service';

export const DinnersRoutes: Routes = [
  {
    path: '',
    component: DinnersComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DinnersRoutes),
    DxButtonModule,
    DxDataGridModule,
    DxLoadPanelModule,
    DxPopupModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxFormModule,
    DxTemplateModule,
    DxSchedulerModule,
    DxListModule,
    DxTextBoxModule,
    DxNumberBoxModule
  ],
  providers: [SuppliersService, DishesService, DinnersService, UsersService],
  declarations: [DinnersComponent]
})
export class DinnersModule { }

