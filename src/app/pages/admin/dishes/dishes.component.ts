import { Component, OnInit } from '@angular/core';
import { DishesService } from '../../../services/http/dishes.service';
import { Dish } from '../../../models/dish';
import { SuppliersService } from '../../../services/http/suppliers.service';
import { Supplier } from '../../../models/supplier';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/zip';

@Component({
  selector: 'app-dishes',
  templateUrl: './dishes.component.html',
  styleUrls: ['./dishes.component.css']
})
export class DishesComponent implements OnInit {

  suppliers: Supplier[];
  dishes: Dish[];
  selectedDish: Dish;
  popupVisible: boolean;
  popupSuppliersVisible: boolean;
  lastRowCLickedId: number;
  expanded: boolean;
  loadingVisible: boolean;
  selectedRow: any;
  checkAll: boolean;
  selectedSuppliers: any[] = [];
  groups: string[] = ['Второе', 'Первое', 'Салат'];
  constructor(private dishesData: DishesService, private suppliersData: SuppliersService) {
    this.refresh();
  }
  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      widget: 'dxButton',
      options: { icon: 'pulldown', onClick: this.refresh.bind(this) },
      location: 'before'
    }, {
        location: 'before',
        widget: 'dxButton',
        options: {
          width: 136,
          text: 'Свернуть',
          onClick: this.collapseAllClick.bind(this)
        }
      }, {
        location: 'before',
        widget: 'dxButton',
        options: {
          width: 136,
          text: 'Добавить',
          onClick: this.addDish.bind(this)
        }
      }, {
        location: 'before',
        widget: 'dxCheckBox',
        options: {
          text: 'Все',
          onValueChanged: this.checkAllClick.bind(this)
        }
      });
  }
  rowClickEvent(e) {
    console.log(e.element);
    const rows = e.component.getSelectedRowsData();
    if (this.lastRowCLickedId === e.rowIndex) {
      this.lastRowCLickedId = e.rowIndex;
      console.log('double');
      rows.then((data) => {
        const row = data[0];
        this.selectedDish = row;
        this.selectedSuppliers = [];
        this.selectedSuppliers.push(row.supplier);
        this.popupVisible = true;
      });
    } else {
      console.log('single');
    }
    this.lastRowCLickedId = e.rowIndex;
  }
  collapseAllClick(e) {
    this.expanded = !this.expanded;
    e.component.option({
      text: this.expanded ? 'Свернуть' : 'Развернуть'
    });
  }
  checkAllClick(e) {
    this.checkAll = e.value;
    this.refresh();
  }
  onSelectedItems(e) {
    console.log(this.selectedSuppliers);
  }
  addDish() {
    this.popupVisible = true;
    this.selectedDish = new Dish();
  }
  saveDish() {
    this.popupVisible = false;
    console.log(this.selectedDish.name);
    if (this.selectedDish.id === 0) {
      this.selectedDish.id = this.dishesData.getNewId();
    } else {
      this.dishes.splice(this.dishes.indexOf(this.selectedDish), 1);
    }
    this.dishes.push(this.selectedDish);
    this.dishesData.saveDishes(this.dishes);
    this.refresh();
  }
  deleteDish() {
    this.dishes.splice(this.dishes.indexOf(this.selectedDish), 1);
    this.popupVisible = false;
  }
  closeDish() {
    this.popupVisible = false;
  }
  addSupplier() {
    if (this.selectedSuppliers.length > 0) { this.selectedDish.supplier = this.selectedSuppliers[0]; }
    this.popupVisible = true;
    this.popupSuppliersVisible = false;
  }
  selectSupplier() {
    this.popupSuppliersVisible = true;
  }
  closeSupplier() {
    this.popupSuppliersVisible = false;
    this.popupVisible = true;
  }
  ngOnInit() {
  }
  refresh() {
    Observable.zip(this.suppliersData.getSuppliers(), this.dishesData.getDishes()).subscribe(([suppliers, dishes]) => {
      this.suppliers = suppliers;
      this.dishes = dishes;
      this.selectedSuppliers = [];
    }
    );
  }
}
