import { Component, OnInit } from '@angular/core';
import { SuppliersService } from '../../../services/http/suppliers.service';
import { Supplier } from '../../../models/supplier';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})
export class SuppliersComponent implements OnInit {

  suppliers: Supplier[];
  selectUser: Supplier;
  popupVisible: boolean;
  constructor(private suppliersData: SuppliersService) {
    this.refresh();
  }
  menuItems = [{
    text: 'Просмотр',
    action: function (e) {
      console.log(e);
    }
  }, {
    text: 'Изменить',
    action: function (e) {
      console.log(e);
    }
  }, {
    text: 'Удалить',
    action: function (e) {
      console.log(e);
    }
  }];

  addUser() {
    this.popupVisible = true;
    this.selectUser = new Supplier();
  }
  saveUser() {
    this.popupVisible = false;
    console.log(this.selectUser.name);
  }
  closeUser() {
    this.popupVisible = false;
  }
  ngOnInit() {
  }
  refresh() {
    this.suppliersData.getSuppliers().subscribe(data => this.suppliers = data);
  }
}
