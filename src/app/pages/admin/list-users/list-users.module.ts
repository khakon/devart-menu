import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListUsersComponent } from './list-users.component';
import {RouterModule, Routes} from '@angular/router';

import { DxTextBoxModule, DxListModule, DxDataGridModule, DxButtonModule,
   DxLoadPanelModule, DxPopupModule, DxSelectBoxModule, DxTextAreaModule,
   DxFormModule, DxSchedulerModule, DxTemplateModule } from 'devextreme-angular';

import { UsersService } from '../../../services/http/users.service';

export const ListUsersRoutes: Routes = [
  {
    path: '',
    component: ListUsersComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListUsersRoutes),
    DxButtonModule,
    DxDataGridModule,
    DxLoadPanelModule,
    DxPopupModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxFormModule,
    DxTemplateModule,
    DxSchedulerModule,
    DxListModule,
    DxTextBoxModule,
  ],
  providers: [UsersService],
  declarations: [ListUsersComponent]
})
export class ListUsersModule { }
