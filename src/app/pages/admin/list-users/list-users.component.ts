import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/http/users.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  users: User[];
  selectUser: User;
  popupVisible: boolean;

  constructor(private usersData: UsersService) {
    this.refresh();
  }
  menuItems = [{
    text: 'Просмотр',
    action: function (e) {
      console.log(e);
    }
  }, {
    text: 'Изменить',
    action: function (e) {
      console.log(e);
    }
  }, {
    text: 'Удалить',
    action: function (e) {
      console.log(e);
    }
  }];

  addUser() {
    this.popupVisible = true;
    this.selectUser = new User();
  }
  saveUser() {
    this.popupVisible = false;
    console.log(this.selectUser.name);
  }
  closeUser() {
    this.popupVisible = false;
  }
  ngOnInit() {
  }
  refresh() {
    this.usersData.getUsers().subscribe(data => this.users = data);
  }
}
