import { User } from '../models/user';

export const ListUsers: User[] = [
  { id: 0, name: 'User1', inactive: false },
  { id: 1, name: 'User2', inactive: false },
  { id: 2, name: 'User3', inactive: false },
  { id: 3, name: 'User4', inactive: false },
  { id: 4, name: 'User5', inactive: false },
  { id: 5, name: 'User6', inactive: false },
  { id: 6, name: 'User7', inactive: false },
  { id: 7, name: 'User8', inactive: false },
  { id: 8, name: 'User9', inactive: false },
  { id: 9, name: 'User10', inactive: false },
  { id: 10, name: 'Admin', inactive: false }
];
