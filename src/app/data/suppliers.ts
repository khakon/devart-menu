import { Supplier } from '../models/supplier';

export const ListSuppliers: Supplier[] = [
  { id: 0, name: 'Supplier1', inactive: false },
  { id: 1, name: 'Supplier2', inactive: false },
  { id: 2, name: 'Supplier3', inactive: false },
  { id: 3, name: 'Supplier4', inactive: false },
  { id: 4, name: 'Supplier5', inactive: false },
  { id: 5, name: 'Supplier6', inactive: false },
  { id: 5, name: 'Supplier7', inactive: false },
  { id: 7, name: 'Supplier8', inactive: false },
  { id: 8, name: 'Supplier9', inactive: false },
  { id: 9, name: 'Supplier10', inactive: false }
];
