import { Dish } from '../models/dish';

export const ListDishes: Dish[] = [
    {
        id: 0, name: 'Dish1', price: 1,
        supplier: { id: 1, name: 'Supplier1', inactive: false },
        inactive: false, group: 'Первое', description: 'Описание'
    },
    {
        id: 1, name: 'Dish2', price: 2,
        supplier: { id: 1, name: 'Supplier1', inactive: false },
        inactive: false, group: 'Второе', description: 'Описание'
    },
    {
        id: 2, name: 'Dish3', price: 3,
        supplier: { id: 1, name: 'Supplier1', inactive: false },
        inactive: false, group: 'Салат', description: 'Описание'
    },
    {
        id: 3, name: 'Dish4', price: 4,
        supplier: { id: 1, name: 'Supplier1', inactive: false },
        inactive: false, group: 'Первое', description: 'Описание'
    },
    {
        id: 4, name: 'Dish5', price: 5,
        supplier: { id: 2, name: 'Supplier1', inactive: false },
        inactive: false, group: 'Второе', description: 'Описание'
    },
    {
        id: 5, name: 'Dish6', price: 1,
        supplier: { id: 2, name: 'Supplier1', inactive: false },
        inactive: false, group: 'Салат', description: 'Описание'
    },
    {
        id: 5, name: 'Dish7', price: 2,
        supplier: { id: 2, name: 'Supplier1', inactive: false },
        inactive: false, group: 'Первое', description: 'Описание'
    },
    {
        id: 7, name: 'Dish8', price: 3,
        supplier: { id: 2, name: 'Supplier1', inactive: false },
        inactive: false, group: 'Второе', description: 'Описание'
    },
    {
        id: 8, name: 'Dish9', price: 4,
        supplier: { id: 3, name: 'Supplier1', inactive: false },
        inactive: false, group: 'Салат', description: 'Описание'
    },
    {
        id: 9, name: 'Dish10', price: 5,
        supplier: { id: 3, name: 'Supplier1', inactive: false },
        inactive: false, group: 'Первое', description: 'Описание'
    }
];
