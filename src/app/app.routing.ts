import { Routes } from '@angular/router';
import { AdminComponent } from './layout/admin/admin.component';
import { UserComponent } from './layout/user/user.component';
import { AuthComponent } from './pages/authentication/auth.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'authentication',
        pathMatch: 'full'
      },
      {
        path: 'authentication',
        loadChildren: './pages/authentication/users-auth/users-auth.module#UsersAuthModule'
      },
    ]
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'menus',
        pathMatch: 'full'
      },
      {
        path: 'users',
        loadChildren: './pages/admin/list-users/list-users.module#ListUsersModule'
      },
      {
        path: 'dinners',
        loadChildren: './pages/admin/dinners/dinners.module#DinnersModule'
      },
      {
        path: 'suppliers',
        loadChildren: './pages/admin/suppliers/suppliers.module#SuppliersModule'
      },
      {
        path: 'dishes',
        loadChildren: './pages/admin/dishes/dishes.module#DishesModule'
      },
      {
        path: 'menus',
        loadChildren: './pages/admin/menu/menu.module#MenuModule'
      }]
  },
  {
    path: 'user',
    component: UserComponent,
    children: [
      {
        path: '',
        redirectTo: 'menus',
        pathMatch: 'full'
      },
      {
        path: 'dinners',
        loadChildren: './pages/user/dinners/dinners.module#DinnersModule'
      },
      {
        path: 'suppliers',
        loadChildren: './pages/user/suppliers/suppliers.module#SuppliersModule'
      },
      {
        path: 'dishes',
        loadChildren: './pages/user/dishes/dishes.module#DishesModule'
      },
      {
        path: 'menus',
        loadChildren: './pages/user/menus/menus.module#MenusModule'
      }]
  }
];
