import { Item } from './item';
import { Supplier } from './supplier';
import { Dish } from './dish';

export class Menu extends Item {
    period: Date;
    day: string;
    week: number;
    supplier: Supplier;
    dishes: Dish[];
    constructor() {
        super();
        this.dishes = [];
    }
}

