import { Item } from './item';


export class Dish extends Item {
    price: number;
    supplier: Item;
    description: string;
    group: string;
    constructor() {
        super();
        this.supplier = new Item();
        this.group = '';
      }
}

