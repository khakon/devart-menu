export class Item {
    id: number;
    name: string;
    inactive: boolean;
    constructor() {
        this.id = 0;
        this.inactive = false;
      }
}
