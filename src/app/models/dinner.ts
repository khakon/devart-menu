import { Item } from './item';
import { User } from './user';
import { Dish } from './dish';
import { Menu } from './menu-data';
import { Supplier } from './supplier';
import * as moment from 'moment';

const daysOfWeek = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'];

export class Dinner extends Item {
    period: Date;
    day: string;
    week: number;
    menuId: number;
    supplier: Supplier;
    dish: Dish;
    client: User;
    quantity: number;
    constructor(menu: Menu, dish: Dish) {
        super();
        this.period = menu.period;
        this.day = menu.day;
        this.week = menu.week;
        this.menuId = menu.id;
        this.supplier = menu.supplier;
        this.dish = dish;
        this.quantity = 1;
    }
}
